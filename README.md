Gamefit was created by AvengersUnlimited to help people learn how to use a linux terminal and use a Raspberry Pi.  
  
Special Thanks to:  
NewsDownload.co.uk for PiMan (http://www.newsdownload.co.uk/pages/RPiPiMan.html)  
InventWithPython.com for the Pygame-based games we used (http://inventwithpython.com/pygame/)  
StackOverflow for assistance with a few bits of code  
GitLab for hosting the project  

Follow these instructions to install:  
1) Clone the Project  
2) chmod +x setup.sh  
3) ./setup.sh  