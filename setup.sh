#!/bin/bash
#Pre-requisites script for Gamefit
#Script built for support with Raspbian (Raspberry Pi Debian Fork)
#https://gitlab.com/AvengersUnlimited/Gamefit

#Reccomend to install on a brand new installation of Raspiban, should you be using any other OS - please follow the
#manual installation instructions on https://gitlab.com/AvengersUnlimited/Gamefit

#Defines ask_input function
ask_input(){
 read -n1 -rsp $'Press any key to continue or Ctrl+C to exit...\n'
}

#Checks if superuser
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root, try sudo installer.sh" 1>&2
   exit 1
fi

clear
#Information about the file
echo The installer will now install the pre-requisites you require.
echo You will require an internet connection for the duration of this setup.
echo All games provided are open source and are available on GitHub.
echo The license for this software is available from {url}
ask_input

#Installs pre-requisites.
echo Updating Software Packages
apt-get update --yes --force-yes
echo Upgrading Software
apt-get upgrade --yes --force-yes
echo Installing New Software
apt-get install figlet toilet dialog zip unzip --yes --force-yes
clear

#Clones AvengersUnlimited
cd /home/pi
mkdir /home/pi/getfit
cd /home/pi/getfit
git clone https://gitlab.com/AvengersUnlimited/Gamefit.git


#Own all files as Pi
chown -R pi:pi /home/pi/getfit

#End Installer
toilet --gay Avengers Unlimited
echo Install completed!
echo The files have been installed to /home/pi/getfit
echo You can run the script by typing in sh /home/pi/getfit/start.sh
echo Please find out more about the project by visiting our GitLab page.
ask_input

#Games Installation
toilet --gay Avengers Unlimited
echo Now installing Games
su pi
sh /home/pi/getfit/games/init.sh

#Software *should* have installed.
#Issues must be reported to our GitLab.
#GPLv3 License