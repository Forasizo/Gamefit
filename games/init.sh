#Install PiMan
wget https://github.com/BirchJD/PiMan/archive/master.zip
unzip master.zip
rm -rf master.zip

#Extract Pygames
unzip pygames.zip -d pygames
rm -rf pygames.zip

#Remove Init Script
rm -rf init.sh